import socket
import sys

def system_err(msg):
	print "ERROR: ", msg
	sys.exit(1)


# ================
# SYSTEM ARGUMENTS
# ================
serv_name = 'localhost' #default host
serv_port = 7770 #default port
h_flag_set = False
p_flag_set = False
args_left = len(sys.argv)-1

if (len(sys.argv) > 6):
	system_err("Too many args")
if (len(sys.argv) < 2):
	system_err("Too few args")

for i,v in enumerate(sys.argv):
	if v == "-h":
		if h_flag_set:
			system_err("-h flag used more than once")
		h_flag_set = True
		args_left -= 1	
		if not args_left:
			system_err("Expected arg for -h flag")
		serv_name = sys.argv[i+1]
		args_left -= 1	
	elif v == "-p":
		if p_flag_set:
			system_err("-p flag used more than once")
		p_flag_set = True
		args_left -= 1	
		if not args_left:
			system_err("Expected arg for -p flag")
		serv_port = int(sys.argv[i+1])
		args_left -= 1	

if args_left < 0:
	system_err("Unexpected error with number of arguments")
if args_left == 0:
	system_err("Expected arg for groupname")
if args_left > 1:
	system_err("Too many args")


# ==================
# SOCKETS/CONNECTION
# ==================
try: 
	serv_ip = socket.gethostbyname(serv_name)
except:
	system_err("Could not find the host")

try:
	client_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
except:
	system_err("Could not create a socket")

try:
	client_sock.connect((serv_ip, serv_port))
except:
	system_err("Could not connect to server with the specified host and port")


# =========
# GET  DATA
# =========
groupname = sys.argv[len(sys.argv)-1]


# ============
# GET ACTION
# ===========
def send(data):
	try:
		client_sock.send(data)
	except:
		system_err("Could not send data to server")
	try:
		resp = client_sock.recv(4096)
	except:
		system_err("Could not get a response from the server")
	if resp[0:5] == "error":
		print resp
		return

	# get message count
	print resp, "messages\n"
	try:
		client_sock.send("ready")
	except:
		system_err("Could not send data to server")

	# get message stream 
	total_data = ""
	while True:
		try:
			resp = client_sock.recv(4096)
			if not resp:
				break
			total_data += resp
		except:
			system_err("Could not get a response from the server")
	print total_data

get_group = "get " + groupname
send(get_group)

client_sock.close()