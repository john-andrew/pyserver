from time import ctime
import string
import socket
import sys
import thread, threading

def system_err(msg):
    print "ERROR: ", msg
    sys.exit(1)


# ================
# SYSTEM ARGUMENTS
# ================
serv_name = 'localhost'
serv_port = 7770 #default port
p_flag_set = False
args_left = len(sys.argv)-1

if (len(sys.argv) > 3):
    system_err("Too many args")

for i,v in enumerate(sys.argv):
    if v == "-p":
        if p_flag_set:
            system_err("-p flag used more than once")
        p_flag_set = True
        args_left -= 1  
        if not args_left:
            system_err("Expected arg for -p flag")
        serv_port = int(sys.argv[i+1])
        args_left -= 1  

if args_left < 0:
    system_err("Unexpected error with number of arguments")
if args_left > 0:
    system_err("Too many args")


# ======
# MODELS
# ======
group_table = {}
message_table = []
msgID_count = 0

class Message():
    def __init__(self, id_key, msg, user, ip, port, date, group):
        self.ID = id_key
        self.msg = msg
        self.user = user
        self.ip = ip
        self.port = port
        self.date = date
        self.group = group 

class Group():
    def __init__(self):
        self.messages = {}
        self.messages_count = 0
    
    def add_message(self, msg):
        self.messages.append(msg)

def add_group(groupname):
    pass



# ==============
# CLIENT THREADS
# ==============
model_lock = threading.Lock()

def client_thread(sock, addr):

    data = sock.recv(4096)

    # ----
    # POST
    # ----
    if data [0:5] == "post ":
        print "Received POST request:", data

        # GROUP
        err_msg = ""
        if len(data) < 7:
            err_msg = "Expected a groupname from data"
        elif data[-1:] != "\n":
            err_msg = "Expected newline at end of data"

        group_slice = data[5:-1]
        for i in group_slice:
            if i not in string.printable:
                err_msg = "Groupname can only contain printables"
                break 
        for i in group_slice:
            for c in string.whitespace:
                if i == c:
                    err_msg = "Groupname must have no whitespace"
                    break

        if err_msg:
            err_msg = "error" + err_msg
            try:
                sock.send(err_msg)
            except socket.error as e:
                system_err(str(e))

            sock.close()
            return

        try:
            sock.send("ok")
            print "Sent ok response for group:", group_slice
        except socket.error as e:
            system_err(str(e))

        with model_lock:
            if not group_slice in group_table:
                new_group = Group()
                group_table[group_slice] = new_group

        data = sock.recv(4096)

        # USER
        if data[0:3] == "id ":
            print "Received user data:", data

            err_msg = ""
            if len(data) < 5:
                err_msg = "Expected a username from data"
            elif not data[-1:] == "\n":
                err_msg = "Expected newline at end of data"

            user_slice = data[3:-1]
            for i in user_slice:
                if i not in string.printable:
                    err_msg = "Username can only contain printables"
                    break 

            if err_msg:
                err_msg = "error" + err_msg
                try:
                    sock.send(err_msg)
                except socket.error as e:
                    system_err(str(e))

                sock.close()
                return
            try:
                sock.send("ok")
                print "Sent ok response for user:", user_slice
            except socket.error as e:
                system_err(str(e))

        total_data = ""
        while True:
            data = sock.recv(4096)
            if not data:
                break
            total_data += data


        # MESSAGE
        print "Received message data:", total_data

        with model_lock:
            print_lock = threading.Lock()
            global msgID_count
            new_message = Message(id_key=msgID_count,
                                  msg=total_data,
                                  user=user_slice,
                                  ip=addr[0],
                                  port=addr[1],
                                  date=ctime(),
                                  group=group_slice)
            message_table.append(new_message)

            target_group = group_table[group_slice]
            target_group.messages[msgID_count] = new_message
            target_group.messages_count += 1
            msgID_count += 1

        sock.close()
        return

    # ----
    # GET
    # ----
    elif data [0:4] == "get ":
        print "Received GET request:", data

        # GROUP
        err_msg = ""
        if len(data) < 6:
            err_msg = "invalid group name"

        group_slice = data[4:]

        with model_lock:
            if not group_table.has_key(group_slice):
                err_msg = "invalid group name"

        if err_msg:
            err_msg = "error: " + err_msg
            try:
                sock.send(err_msg)
            except socket.error as e:
                system_err(str(e))
            sock.close()
            return

        with model_lock:
            target_group = group_table[group_slice]
            try:
                sock.send(str(target_group.messages_count))
                print "Sent message count", target_group.messages_count
            except socket.error as e:
                system_err(str(e))

        # MESSAGE
        ready_protocol = ""
        while (ready_protocol != "ready"):
            ready_protocol = sock.recv(4096)

        stream = ""
        counter = 0;

        with model_lock:
            for i in target_group.messages:
                # print target_group.messages[i].msg

                x = target_group.messages[i]

                # header_data
                header_data = "From "
                header_data += x.user
                header_data += " /"
                header_data += x.ip
                header_data += ":"
                header_data += str(x.port)
                header_data += " "
                header_data += x.date
                header_data += "\n\n"
                stream += header_data

                # message_data
                message_data = x.msg
                if not (counter == len(target_group.messages) - 1):
                    message_data += "\n\n"
                stream += message_data

                counter += 1

        try:
            sock.send(stream)
            print "Sending GET response stream:", stream
        except socket.error as e:
            system_err(str(e))

        sock.close()
        return

    # -------
    # INVALID (not GET or POST)
    # -------
    else:
        try:
            sock.send("errorinvalid command")
            print "Received an invalid command, sending error message"
        except socket.error as e:
            system_err(str(e))

        sock.close()
        return


# ==================
# SOCKETS/CONNECTION
# ==================

try: 
    serv_ip = socket.gethostbyname(serv_name)
except socket.error as e:
    system_err(str(e))

try:
    serv_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
except socket.error as e:
    system_err(str(e))
print "Server socket created"

try:
    serv_sock.bind((serv_ip, serv_port))
except socket.error as e:
    system_err(str(e))
print "Server socket binded"

print "Server now running, host/ip/port: " + serv_name + "/" + \
    serv_ip + "/" + str(serv_port)

serv_sock.listen(100)
print "Server now listening for connections"

while True:
    conn_sock, conn_addr = serv_sock.accept()
    print "Received connection"

    thread.start_new_thread(client_thread, (conn_sock, conn_addr,))

conn_sock.close()